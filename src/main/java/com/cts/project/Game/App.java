package com.cts.project.Game;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cts.project.model.Team;

public class App 
{
    public static void main( String[] args )
    {
    	/*
    	 * Collection injection
    	 */
    	ApplicationContext ctx = new ClassPathXmlApplicationContext("configure.xml");
    	Team RCB = (Team) ctx.getBean("RCB");
    	RCB.display();
    }
}
