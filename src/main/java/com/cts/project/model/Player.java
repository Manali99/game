package com.cts.project.model;

public class Player {
	private int playerId;
	private String playerName;
	private String playerSkill;	
	private String playerCountry;
	
	
	public int getPlayerId() {
		return playerId;
	}



	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}



	public String getPlayerName() {
		return playerName;
	}



	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}



	public String getPlayerSkill() {
		return playerSkill;
	}



	public void setPlayerSkill(String playerSkill) {
		this.playerSkill = playerSkill;
	}



	public String getPlayerCountry() {
		return playerCountry;
	}



	public void setPlayerCountry(String playerCountry) {
		this.playerCountry = playerCountry;
	}



	public String toString()
	{
		return String.format("%-5s  %-20s  %-15s  %s",playerId,playerName,playerSkill,playerCountry);
	}
}
